---
title: Why error handling is important with promise and how to do it
---

Callback hell and the inversion of control are the two main reason which makes the asynchronous code difficult to understand and debug, to overcome these kinds of problems later promises are introduced promise providing a more cleaner and structured way to write the asynchronous code which increases the readability and the helps us to control the flow of program more efficiently than the callbacks.

Every promise object has 2 possible states ( fulfilled and rejected). There is a possibility that the promise will take the rejection in its final state, which leads to the error console on the consumer or the user system if it's not handled properly, so it becomes very important for the developer to handle all errors gracefully.

## Doing Error Handling with promise gives us the following benefit.

- **User Experience Improved:-** if an error is not handled correctly it causes an error consoled on the screen of the user or the consumer that leads to a bad user experience, so that's why it becomes necessary to handle all expected errors gracefully to improve the user experience.

- **Improve readability:-** the code written using promise grows horizontally which makes the code more readable and understandable.

- **Enhancing code maintainability:-** By ensuring that your code is well-structured and simple to understand, proper error handling can make it simpler to maintain. In the long term, this can save time and money by making it simpler to maintain and modify your code as necessary.

## Ways to throw errors in the promise:

- **Using reject():-** reject is one of the arguments passed in the callback of the promise we can throw an error in the reject when the promise fails.

```jsx
return new Promise((resolve, reject) => {
        if (!authorized) {
            reject('Unauthorized access to the user data');
        }}
```

- **Using throw:-** inside the promise, we can also use throw error by

```jsx
return new Promise((resolve, reject) => {
    if (!authorized) {
        throw new Error("Unauthorized access to the user data");
}}
```

**_Error thrown by both methods is cached by the .catch()._**

## How to handle errors using catch().

Whenever the error is thrown the control of the program is immediately transferred to the catch block and it skips the all then block between them.

### Using single catch().

**Code Example:-**

```jsx
function fetchAllUser() {
  const userUrl = "https://jsonplaceholder.typicode.com/userss";

  fetch(userUrl)
    .then(function (userUrlResponse) {
      if (!userUrlResponse.ok) {
        throw new Error("resopnse not recived");
      } else {
        return userUrlResponse.json();
      }
    })
    .then(function (userUrlResponse) {
      console.log(userUrlResponse);
    })
    .catch(function (error) {
      console.log(error);
    });
}
```

In the above code example, there is an error arising in the fetch due to the wrong URL, so the control of the program is directly transferred to the catch block.

The placement of the catch block is the developer call based on the requirement of the program If we go with only one catch most commonly at the end of the program all the statements between the point of error arise and the nearest catch block is to be skipped. As shown in the above example after getting an error in the fetch the control directly goes to the catch statement and throws an error.

### using multiple catch block:-

```jsx
function fetchAllUser() {
  const userUrl = "https://jsonplaceholder.typicode.com/userss";
  fetch(userUrl)
    .then(function (userUrlResponse) {
      if (!userUrlResponse.ok) {
        throw new Error("resopnse not received");
      } else {
        return userUrlResponse.json();
      }
    })
    .catch(function (error) {
      console.log(error);
    })
    .then(function () {
      console.log("program execution not terminated");
    })
    .catch(function (error) {
      console.log(error);
    });
}
```

In the above code we use multiple .catch(), and as result, we see that the program flow is not terminated.

### Missing catch().

If you miss the catch block when working with promises in JavaScript, it can cause your code to silently fail without any indication to the developer. This can be difficult to debug and can lead to unexpected behavior in your code.
